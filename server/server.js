const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");

const packageDef = protoLoader.loadSync("todo.proto", {});
const grpcObj = grpc.loadPackageDefinition(packageDef);
const todoPackage = grpcObj.todoPackage;

/* const todos = [
  { id: 1, text: "Una tarea de prueba!!!" },
  { id: 2, text: "Buy groceries" },
  { id: 3, text: "Prueba de tarea"}
]; */

const todos = [
  { id: 1, text: "Una tarea de prueba!!!" },
  { id: 2, text: "Buy groceries" },
  { id: 3, text: "Prueba de tarea" },
  { id: 4, text: "Tarea 4" },
  { id: 5, text: "Tarea 5" },
  { id: 6, text: "Tarea 6" },
  { id: 7, text: "Tarea 7" },
  { id: 8, text: "Tarea 8" },
  { id: 9, text: "Tarea 9" },
  { id: 10, text: "Tarea 10" },
  { id: 11, text: "Tarea 11" },
  { id: 12, text: "Tarea 12" },
  { id: 13, text: "Tarea 13" },
  { id: 14, text: "Tarea 14" },
  { id: 15, text: "Tarea 15" },
  { id: 16, text: "Tarea 16" },
  { id: 17, text: "Tarea 17" },
  { id: 18, text: "Tarea 18" },
  { id: 19, text: "Tarea 19" },
  { id: 20, text: "Tarea 20" },
  { id: 21, text: "Tarea 21" },
  { id: 22, text: "Tarea 22" },
  { id: 23, text: "Tarea 23" },
  { id: 24, text: "Tarea 24" },
  { id: 25, text: "Tarea 25" },
  { id: 26, text: "Tarea 26" },
  { id: 27, text: "Tarea 27" },
  { id: 28, text: "Tarea 28" },
  { id: 29, text: "Tarea 29" },
  { id: 30, text: "Tarea 30" },
  { id: 31, text: "Tarea 31" },
  { id: 32, text: "Tarea 32" },
  { id: 33, text: "Tarea 33" },
  { id: 34, text: "Tarea 34" },
  { id: 35, text: "Tarea 35" },
  { id: 36, text: "Tarea 36" },
  { id: 37, text: "Tarea 37" },
  { id: 38, text: "Tarea 38" },
  { id: 39, text: "Tarea 39" },
  { id: 40, text: "Tarea 40" },
  { id: 41, text: "Tarea 41" },
  { id: 42, text: "Tarea 42" },
  { id: 43, text: "Tarea 43" },
  { id: 44, text: "Tarea 44" },
  { id: 45, text: "Tarea 45" },
  { id: 46, text: "Tarea 46" },
  { id: 47, text: "Tarea 47" },
  { id: 48, text: "Tarea 48" },
  { id: 49, text: "Tarea 49" },
  { id: 50, text: "Tarea 50" },
];

function createTodo(call, callback) {
  const todoItem = {
    id: todos.length + 1,
    text: call.request.text,
  };
  todos.push(todoItem);
  callback(null, todoItem);
}

let countSexy = 0;
function readTodos(call, callback) {
  callback(null, { items: todos });
  countSexy += 1;

  console.clear();
  console.log(countSexy);
}

async function readTodosStream(call, callback) {
  const batchSize = 1; // Tamaño del lote
  const totalElements = todos.length;
  let startIndex = 0;

  while (startIndex < totalElements) {
    const endIndex = Math.min(startIndex + batchSize, totalElements);
    const batch = todos.slice(startIndex, endIndex);

    for (const todo of batch) {
      call.write(todo);
    }

    startIndex += batchSize;

    await new Promise((resolve) => setTimeout(resolve, 100));
  }

  call.end(); // Señal de fin de la transmisión
}

const server = new grpc.Server();
server.addService(todoPackage.Todo.service, {
  createTodo: createTodo,
  readTodos: readTodos,
  readTodosStream: readTodosStream,
});

server.bindAsync(
  "0.0.0.0:40000",
  grpc.ServerCredentials.createInsecure(),
  (err) => {
    console.clear();
    err ? console.log(err) : "";
    console.log("> Se inicio Bro...");
  }
);
