const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");

const packageDef = protoLoader.loadSync("todo.proto", {});
const grpcObj = grpc.loadPackageDefinition(packageDef);
const todoPackage = grpcObj.todoPackage;

const client = new todoPackage.Todo(
  "localhost:40000",
  grpc.credentials.createInsecure()
);

/* client.readTodos({}, (err, response) => {
  console.log("Todos: " + JSON.stringify(response));
}); */

async function readTodos() {
  try {
    await client.readTodos({}, (err, response) => {
      console.log(JSON.stringify(response));
    });
  } catch (error) {
    console.error("Error reading todos:", error);
  }
}

/* async function readTodosStream() {
  const call = client.readTodosStream();

  await call.on("data", (item) => {
    console.log("ID:", item.id);
    console.log("Texto:", item.text);
  }); 

  call.on("end", () => {
    console.log("Server done.");
  });
} */

async function readTodosStream() {
  const call = client.readTodosStream(); 
  
  await call.on("data", (item) => {
    console.log("ID:", item.id, item.text); 
  }); 

  call.on("end", () => {
    console.log("Servidor ha terminado de enviar los elementos.");
  }); 

  call.on("error", (error) => { 
    console.error("Error en la transmisión:", error);
  });
}

// Inicio cliente -----------------------------------
console.clear() 
console.log("Sistema de pruebas - Cliente gRPC");
readTodosStream();
