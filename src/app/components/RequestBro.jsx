"use client";
export const fetchCache = "only-no-store";

import { useState, useEffect } from "react";


export const RequestBro = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const eventSource = new EventSource("/api/example");

    eventSource.onmessage = (event) => {
      const todo = JSON.parse(event.data);
      setData((prevData) => [...prevData, todo]); // Usando el callback de actualización del estado
      console.log(`Tarea: ${todo.id} - ${todo.text}`);
    };

    eventSource.onerror = () => {
      eventSource.close();
    };

    return () => {
      eventSource.close();
    };
  }, []);

  return (
    <div>
      <h1>Todos:</h1>
      <ul>
        {data.map((item, index) => (
          <li key={index}>
            <strong>{item.id}</strong> - {item.text}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default RequestBro;

/* import { useState, useEffect } from "react";

const Sexy = () => {
  const [message, setMessage] = useState("");

  // eslint-disable-next-line react-hooks/exhaustive-deps
  let headersList = {
    Accept: "/*",
  };

  useEffect(() => {
    async function fetchData() {
      let response = await fetch("/api/example", {
        method: "GET",
        headers: headersList,
      });

      let data = await response.json();
      setMessage(`The city Karen is from ${data.items[2].text}`);
    }

    fetchData();
  }, [headersList]);

  return (
    <div>
      <h1>{message}</h1>
    </div>
  );
};

export default Sexy; */
