'use server'

const readTodos = () =>
  new Promise((resolve, reject) => {
    try {
      client.readTodos({}, (err: any, response: object) => {
        if (err) {
          reject(err);
        } else {
          resolve(response);
        }
      });
    } catch (error) {
      reject(error);
    }
  });

interface TodoItem {
  id: number;
  text: string;
}

import { Readable } from "stream";
import { NextResponse } from "next/server";

const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");

const packageDef = protoLoader.loadSync("todo.proto", {});
const grpcObj = grpc.loadPackageDefinition(packageDef);
const todoPackage = grpcObj.todoPackage;

const client = new todoPackage.Todo(
  "localhost:40000",
  grpc.credentials.createInsecure()
);

async function readTodosStream() {
  const result = new Readable({
    read() {},
  });

  const call = client.readTodosStream();

  call.on("data", (item: string) => {
    result.push(`data: ${JSON.stringify(item)}\n\n`);
  });

  call.on("end", () => {
    result.push(null);
  });

  call.on("error", (error: any) => {
    console.error("Error reading todos stream:", error);
    result.destroy(error);
  });

  return result;
}

export async function GET() {
  try {
    const data = await readTodosStream();

    const stream = new ReadableStream({
      start(controller) {
        data.on("data", (chunk) => {
          controller.enqueue(chunk);
        });

        data.on("end", () => {
          controller.close();
        });

        data.on("error", (error: any) => {
          controller.error(error);
        });
      },
    });

    const response = new NextResponse(stream, {
      status: 200,
      headers: {
        "Content-Type": "text/event-stream; charset=utf-8",
        "Cache-Control": "no-store",
        Connection: "keep-alive",
        "Content-Encoding": "none",
      },
    });

    return response;
  } catch (error: any) {
    console.error("Error reading todos:", error);
    return new NextResponse(JSON.stringify({ error }), { status: 500 });
  }
}









/* const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");
const { Readable } = require("stream");

const packageDef = protoLoader.loadSync("todo.proto", {});
const grpcObj = grpc.loadPackageDefinition(packageDef);
const todoPackage = grpcObj.todoPackage;

const client = new todoPackage.Todo(
  "localhost:40000",
  grpc.credentials.createInsecure()
);

async function readTodosStream() {
  const result = new Readable({
    read() {},
  });

  const call = client.readTodosStream();

  call.on("data", (item: string) => {
    result.push(`data: ${JSON.stringify(item)}\n\n`);
  });

  call.on("end", () => {
    result.push(null);
  });

  call.on("error", (error: any) => {
    console.error("Error reading todos stream:", error);
    result.destroy(error);
  });

  return result;
}

export const GET = async () => {
  try {
    const data = await readTodosStream();

    return new Response(data, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Cache-Control": "no-store",
        Connection: "keep-alive",
        "Content-Encoding": "none",
        "Content-Type": "text/event-stream; charset=utf-8",
      },
    });
  } catch (error: any) {
    console.error("Error reading todos:", error);
    return new Response(JSON.stringify({ error }), {
      status: 500,
      headers: { "Content-Type": "application/json" },
    });
  }
}; */
